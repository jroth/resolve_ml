import configparser
import nifty8 as ift
import resolve as rve

N_samples = 10000
ift.random.push_sseq_from_seed(42)

cfg = configparser.ConfigParser()
cfg.read("conf.cfg")
sky_diffuse, pl_ops = rve.sky_model_diffuse(cfg["sky"])

for i in range(N_samples):
    rd_pos = ift.from_random(sky_diffuse.domain)
    smp = sky_diffuse(rd_pos)
    rve.ubik_tools.field2fits(smp, f'sky_{i}.fits')
